package org.beetl.sql.core.db;

import java.util.Map;

public class SqlServerStyle extends AbstractDBStyle {

	public SqlServerStyle() {
	}


	@Override
	public String getPageSQL(String sql) {
		return "with query as ( select inner_query.*, row_number() over (order by current_timestamp) as beetl_rn from ( "
				+ sql.replaceFirst("(?i)select", "select top("+HOLDER_START+PAGE_END+HOLDER_END+") ")+ this.getOrderBy()
				+" ) inner_query ) select * from query where beetl_rn between "+HOLDER_START+OFFSET+HOLDER_END+" and "+HOLDER_START+PAGE_END+HOLDER_END;
	}


	
	@Override
	public void initPagePara(Map<String, Object> paras,long start,long size) {
		long s = start+(this.offsetStartZero?1:0);
		paras.put(DBStyle.OFFSET,s);
		paras.put(DBStyle.PAGE_END,size);
	}



	@Override
	public String getName() {
		return "sqlserver";
	}
	
	@Override
	public String getEscapeForKeyWord(){
		return "";
	}

}
